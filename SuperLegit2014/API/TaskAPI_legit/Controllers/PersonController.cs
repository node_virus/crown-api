﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TaskAPI_legit.Models;
using TaskDB.domain.Models;
using Newtonsoft.Json;

namespace TaskAPI_legit.Controllers
{ 
    public class PersonController : Controller
    {

        [HttpGet]
        public string Index()
        {
            using (var db = new TaskDBContext())
            {
                var person = db.PersonLists.Select(x => new PersonDTO()
                {
                    ID = x.ID,
                    name = x.name
                }).ToList();
                return JsonConvert.SerializeObject(person);
            }
        }

        [HttpPost]
        public ActionResult Create(PersonDTO person)
        {
            using (var db = new TaskDBContext())
            {
                if (ModelState.IsValid)
                {
                    PersonList t = new PersonList();
                    t.name=person.name;
                    if (t.name != null)
                    {
                        db.PersonLists.Add(t);
                        db.SaveChanges();

                        return Json(new Error { errorDes="Added"});
                    }
                    else
                    {
                        return Json(new Error { errorDes="name cannot be empty"});
                    }
                }

                return Json(new Error { errorDes = "Invalid model" });
            }
        }

        [HttpGet]
        public ActionResult Get(int id)
        {
            using (var db = new TaskDBContext())
            {
                PersonList person = db.PersonLists.Find(id);
                var model = new PersonDTO
                {
                    ID = person.ID,
                    name = person.name
                };
                return Json(model, JsonRequestBehavior.AllowGet);
            }
        }

        //
        // POST: /Default1/Edit/5

        [HttpPost]
        public ActionResult Edit(PersonDTO person)
        {
            using (var db = new TaskDBContext())
            {
                if (ModelState.IsValid)
                {
                    PersonList personToChange = db.PersonLists.Find(person.ID);
                    personToChange.name = person.name;
                    db.Entry(personToChange).State = EntityState.Modified;
                    db.SaveChanges();
                    return Json(1);
                }
                else
                {
                    return Json(1);
                }
            }
        }
        //
        // POST: /Default1/Delete/5

        [HttpPost]
        public ActionResult Delete(int id)
        {
            try
            {
                using (var db = new TaskDBContext())
                {
                    PersonList person = db.PersonLists.Find(id);
                    var taskList = db.TaskLists.Where(x => x.personID == id).Select(x => new TaskDTO()
                        {
                            Title = x.Title,
                            personID = (int)x.personID,
                            StartDate = (DateTime)x.StartDate,
                            EndDate = (DateTime)x.EndDate,
                            ID = x.ID
                        }).ToList();
                    if (taskList.Count() > 0)
                    {
                        return Json(new Error { errorDes = "2" });
                    }
                    db.PersonLists.Remove(person);
                    db.SaveChanges();
                    return Json(new Error { errorDes = "1" });
                }
            }
            catch (Exception e)
            {
                return Json(e.Message);
            }
        }

        protected override void Dispose(bool disposing)
        {
            using (var db = new TaskDBContext())
            {
                db.Dispose();
                base.Dispose(disposing);
            }
        }
    }
}