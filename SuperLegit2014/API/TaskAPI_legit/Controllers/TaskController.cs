﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TaskAPI_legit.Models;
using TaskDB.domain.Models;
using System.Collections.Specialized;
using Newtonsoft.Json;

namespace TaskAPI_legit.Controllers
{ 
    public class TaskController : Controller
    {
       
    
        //
        // GET: /Task/

        public string Index()
        {
            using (var db = new TaskDBContext())
            {
                var tasks = db.TaskLists.Select(x => new TaskDTO()
                {
                    ID = x.ID,
                    Title = x.Title,
                    StartDate = (DateTime)x.StartDate,
                    EndDate = (DateTime)x.EndDate,
                    personID = (int)x.personID,
                    person = new PersonDTO()
                    {
                        ID = x.PersonList.ID,
                        name = x.PersonList.name
                    }
                }).ToList();

                return JsonConvert.SerializeObject(tasks);
            }
        }

        //
        // GET: /Task/Details/5

        public ViewResult Details(int id)
        {
            using (var db = new TaskDBContext())
            {
                TaskList task = db.TaskLists.Find(id);
                return View(task);
            }
        }

        //
        // GET: /Task/Create

        public ActionResult Create()
        {
            return View();
        } 

        //
        // POST: /Task/Create

        [HttpPost]
        public ActionResult Create(TaskDTO task)
        {
            using (var db = new TaskDBContext())
            {
                if (ModelState.IsValid)
                {
                    /*var model = new Task{
                       Title=task.Title,
                       StartDate=task.StartDate
                    };*/

                    if (db.PersonLists.Any(o => o.ID == task.personID))
                    {
                        TaskList t=new TaskList();
                        t.ID = task.ID;
                        t.StartDate = task.StartDate;
                        t.EndDate = task.EndDate;
                        t.Title = task.Title;
                        t.personID = task.personID;
                        db.TaskLists.Add(t);
                        db.SaveChanges();
                        return Json("Success");
                    }
                    else
                    {
                        return Json("PersonID " + task.personID + " does not exist");

                    }
                }
                else
                {
                    return Json("Invalid Model");
                }
            }
        }
       
        

      
    
        
        //
        // GET: /Task/Edit/5
        [HttpGet]
        public string Get(int id)
        {
            using (var db = new TaskDBContext())
            {
                if (db.TaskLists.Any(o => o.ID == id))
                {
                    TaskList task = db.TaskLists.Find(id);
                    PersonList person = db.PersonLists.Find(task.personID);
                    TaskDTO getTask = new TaskDTO()
                    {
                        ID = task.ID,
                        Title = task.Title,
                        StartDate = (DateTime)task.StartDate,
                        EndDate = (DateTime)task.EndDate,
                        personID = (int)task.personID,
                        person = new PersonDTO()
                        {
                            ID = person.ID,
                            name = person.name
                        }
                    };

                    return JsonConvert.SerializeObject(getTask);

                }
                else
                {
                    return JsonConvert.SerializeObject("Task does not exist");
                }
            }
        }

        //
        // POST: /Task/Edit/5

        [HttpPost]
        public ActionResult Edit(TaskDTO task)
        {
            using (var db = new TaskDBContext())
            {
                if (ModelState.IsValid)
                {
                    if (db.TaskLists.Any(o => o.ID == task.ID))
                    {
                        NameValueCollection param = Request.Params;
                        TaskList taskToChange = db.TaskLists.Find(task.ID);
                        
                        if(param.Get("title") != null)
                            taskToChange.Title = task.Title;
                        if (param.Get("startdate") != null)
                            taskToChange.StartDate = task.StartDate;
                        if(param.Get("enddate") != null)
                            taskToChange.EndDate = task.EndDate;
                        if (param.Get("personID") != null && db.PersonLists.Find(Convert.ToInt32(param.Get("personID"))) != null)
                            taskToChange.personID = task.personID;
                        db.Entry(taskToChange).State = EntityState.Modified;

                        db.SaveChanges();
                        return Json("Edited");
                    }
                    else
                    {
                        return Json("Task does not exist");
                    }
                }
                return View(task);
            }
        }


        [HttpPost]
        public ActionResult Delete(int id)
        {
            try
            {
                using (var db = new TaskDBContext())
                {
                    TaskList task = db.TaskLists.Find(id);
                    db.TaskLists.Remove(task);
                    db.SaveChanges();
                    return Json("success");
                }
            }
            catch (Exception e)
            {
                return Json(e.Message);
            }
        }

        [HttpGet]
        public string GetTaskByOwner(int id)
        {
            using (var db = new TaskDBContext())
            {
                var tasks = db.TaskLists.Where(x => x.personID == id).Select(x => new TaskDTO()
                    {
                        ID = x.ID,
                        StartDate = (DateTime)x.StartDate,
                        EndDate = (DateTime)x.EndDate,
                        Title = x.Title,
                        personID = (int)x.personID
                    }).ToList();
                return JsonConvert.SerializeObject(tasks);
            }
        }

        protected override void Dispose(bool disposing)
        {
            using (var db = new TaskDBContext())
            {
                db.Dispose();
                base.Dispose(disposing);
            }
        }
    }
}