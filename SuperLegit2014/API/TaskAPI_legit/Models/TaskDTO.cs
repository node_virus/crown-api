﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data.Entity;

namespace TaskAPI_legit.Models
{
    public class TaskDTO
    {
        public int ID {get;set;}
        public string Title {get; set;}
        public DateTime StartDate {get; set;}
        public DateTime EndDate { get; set; }
        public int personID { get; set; }
        public virtual PersonDTO person { get; set; }
    }
  
}