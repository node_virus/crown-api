using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace TaskDB.domain.Models.Mapping
{
    public class TaskListMap : EntityTypeConfiguration<TaskList>
    {
        public TaskListMap()
        {
            // Primary Key
            this.HasKey(t => t.ID);

            // Properties
            this.Property(t => t.Title)
                .HasMaxLength(50);

            // Table & Column Mappings
            this.ToTable("TaskList");
            this.Property(t => t.ID).HasColumnName("ID");
            this.Property(t => t.StartDate).HasColumnName("StartDate");
            this.Property(t => t.EndDate).HasColumnName("EndDate");
            this.Property(t => t.Title).HasColumnName("Title");
            this.Property(t => t.personID).HasColumnName("personID");

            // Relationships
            this.HasOptional(t => t.PersonList)
                .WithMany(t => t.TaskLists)
                .HasForeignKey(d => d.personID);

        }
    }
}
