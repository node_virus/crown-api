using System;
using System.Collections.Generic;

namespace TaskDB.domain.Models
{
    public partial class PersonList
    {
        public PersonList()
        {
            this.TaskLists = new List<TaskList>();
        }

        public int ID { get; set; }
        public string name { get; set; }
        public virtual ICollection<TaskList> TaskLists { get; set; }
    }
}
