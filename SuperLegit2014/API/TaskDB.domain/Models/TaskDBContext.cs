using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using TaskDB.domain.Models.Mapping;

namespace TaskDB.domain.Models
{
    public partial class TaskDBContext : DbContext
    {
        static TaskDBContext()
        {
            Database.SetInitializer<TaskDBContext>(null);
        }

        public TaskDBContext()
            : base("Name=TaskDBContext")
        {
        }

        public DbSet<PersonList> PersonLists { get; set; }
        public DbSet<TaskList> TaskLists { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new PersonListMap());
            modelBuilder.Configurations.Add(new TaskListMap());
        }
    }
}
