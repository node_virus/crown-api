using System;
using System.Collections.Generic;

namespace TaskDB.domain.Models
{
    public partial class TaskList
    {
        public int ID { get; set; }
        public Nullable<System.DateTime> StartDate { get; set; }
        public Nullable<System.DateTime> EndDate { get; set; }
        public string Title { get; set; }
        public Nullable<int> personID { get; set; }
        public virtual PersonList PersonList { get; set; }
    }
}
