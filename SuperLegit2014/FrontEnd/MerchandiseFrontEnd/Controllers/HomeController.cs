﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Text;
using System.Collections.Specialized;
using MerchandiseFrontEnd.Models;
using System.Configuration;
using log4net;

namespace MerchandiseFrontEnd.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            LogManager.GetLogger("test").Debug("test");
            return View();
        }

        public ActionResult desk()
        {
            return View();
        }

        public string PersonListView()
        {
            var url = ConfigurationManager.AppSettings["url"].ToString();
            LogManager.GetLogger("test").Debug("test");
            
            return HttpGet("http://crownapi.azurewebsites.net/person", "", "");
        }

        public string TaskListView()
        {
            return HttpGet("http://crownapi.azurewebsites.net/task", "", "");
        }

        public string PersonDetailView(int id)
        {
            return HttpGet("http://crownapi.azurewebsites.net/task/GetTaskByOwner/" + id, "", "");
        }
        public string createTask(TaskDto newTask)
        {
            using (WebClient client = new WebClient())
            {
                byte[] response = client.UploadValues("http://crownapi.azurewebsites.net/task/create", new NameValueCollection()
               {
                   { "personID", Convert.ToString(newTask.personID) },
                   { "Title", newTask.Title },
                   { "StartDate", Convert.ToString(newTask.StartDate) },
                   { "EndDate", Convert.ToString(newTask.EndDate) }
               });
            }
            return "";
        }
        public string createPerson(PersonDto newPerson)
        {
            using (WebClient client = new WebClient())
            {
                byte[] response = client.UploadValues("http://crownapi.azurewebsites.net/person/create", new NameValueCollection()
               {
                   { "name", newPerson.name }
               });
            }
            return "";
        }

        public string editPerson(PersonDto newPerson)
        {
            using (WebClient client = new WebClient())
            {
                byte[] response = client.UploadValues("http://crownapi.azurewebsites.net/person/edit", new NameValueCollection()
               {
                   { "ID", Convert.ToString(newPerson.ID) },
                   { "name", newPerson.name}
                   
               });
            }
            return "";
        }

        public string deletePerson(PersonDto newPerson)
        {
            using (WebClient client = new WebClient())
            {
                byte[] response = client.UploadValues("http://crownapi.azurewebsites.net/person/delete", new NameValueCollection()
                {
                    { "ID", Convert.ToString(newPerson.ID) }
                });

                string responseString = Encoding.ASCII.GetString(response);
                Error jsonResponse = JsonConvert.DeserializeObject<Error>(responseString);
                if (jsonResponse.errorDes == "2")
                {
                    return "Current person is assigned to one or more tasks, please complete the tasks before deleting the person";
                }

            }
            return "";
        }

        public string editTask(TaskDto newTask)
        {
            using (WebClient client = new WebClient())
            {
                byte[] response = client.UploadValues("http://crownapi.azurewebsites.net/task/edit", new NameValueCollection()
               {
                   { "ID", Convert.ToString(newTask.ID) },
                   { "Title", newTask.Title },
                   { "StartDate", Convert.ToString(newTask.StartDate) },
                   { "EndDate", Convert.ToString(newTask.EndDate) }
               });
            }
            return "";
        }

        public string deleteTask()
        {
            using (WebClient client = new WebClient())
            {
                string[] idList = Request.Form.GetValues("id");
                for (int i = 0; i < idList.Count(); i++)
                {
                    byte[] response = client.UploadValues("http://crownapi.azurewebsites.net/task/delete", new NameValueCollection()
                   {
                       { "ID", Convert.ToString(idList[i]) }
                   });
                }
            }
            return "";
        }

        [HttpGet]
        public string HttpGet(string uri, string username, string password)
        {
            Stream stream;
            StreamReader reader;
            String response = null;
            WebClient webClient = new WebClient();

            using (webClient)
            {
                // Check our auth details
                if (!string.IsNullOrEmpty(username) && !string.IsNullOrEmpty(password))
                {
                    webClient.Credentials = new NetworkCredential(username, password);
                }

                try
                {
                    // open and read from the supplied URI
                    stream = webClient.OpenRead(uri);
                    reader = new StreamReader(stream);
                    response = reader.ReadToEnd();
                }
                catch (WebException ex)
                {
                    if (ex.Response is HttpWebResponse)
                    {
                        // Add you own error handling as required
                        switch (((HttpWebResponse)ex.Response).StatusCode)
                        {
                            case HttpStatusCode.NotFound:
                            case HttpStatusCode.Unauthorized:
                                response = null;
                                break;

                            default:
                                throw ex;
                        }
                    }
                }
            }

            return response;
        }
    }
}
