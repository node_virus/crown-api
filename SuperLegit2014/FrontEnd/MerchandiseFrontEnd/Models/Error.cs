﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace MerchandiseFrontEnd.Models
{
    public class Error
    {
        [JsonProperty(PropertyName = "errorDes")]
        public string errorDes { get; set; }
    }
}