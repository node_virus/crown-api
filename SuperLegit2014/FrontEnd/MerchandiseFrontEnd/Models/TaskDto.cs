﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MerchandiseFrontEnd.Models
{
    public class TaskDto
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int personID { get; set; }
        public virtual PersonDto person { get; set; }
    }
}