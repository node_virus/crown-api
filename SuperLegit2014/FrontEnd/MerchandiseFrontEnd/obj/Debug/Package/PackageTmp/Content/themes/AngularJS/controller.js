var app = angular.module('myApp', ['ngGrid']);
app.controller('PersonCtrl', function ($scope, $http) {
    $scope.refresh = function () {
        if (!$scope.$$phase) {
            $http.get('http://crown.azurewebsites.net/home/PersonListView').success(function (data, status, headers, config) {
                $scope.myData = data;
                $scope.apply();

            }).error(function (data, status, headers, config) {
                alert("fail");
            });
        }
    }
    $http.defaults.useXDomain = true;
    $http.get('http://crown.azurewebsites.net/home/PersonListView').success(function (data, status, headers, config) {
        $scope.myData = data;

    }).error(function (data, status, headers, config) {
        alert("fail");
    });
    $scope.gridOptions = {
        data: 'myData',
        enableCellSelection: false,
        enableRowSelection: false,
        columnDefs: [
            { field: '', displayName: '', width: 40 },
            { field: 'ID', displayName: 'ID' },
            { field: 'name', displayName: 'Name' },
            { field: '', displayName: 'Options', cellTemplate:
            '<button class="btn btn-default delete-btn" ng-click="ngDeletePerson(row)"><span class="glyphicon glyphicon-remove"></span></button> \
            <button class="btn btn-default detail-btn" data-toggle="modal" data-target="#detailPersonModal" ng-click="ngDetailPerson(row)"><span class="glyphicon glyphicon-eye-open"></span></button> \
            <button class="btn btn-default detail-btn" data-toggle="modal" data-target="#addTaskModal" ng-click="ngAddTask(row)"><span class="glyphicon glyphicon-plus"></span></button> \
            <button class="btn btn-default detail-btn" data-toggle="modal" data-target="#editPersonModal" ng-click="ngEditPerson(row)"><span class="glyphicon glyphicon-pencil"></span></button>'
            }
        ]
    };
    $scope.ngDeletePerson = function (row) {
        callController("deletePerson", "POST", "ID=" + row.entity.ID, function (data) {
            if (data == "") {
                angular.element($("#personGrid")).scope().refresh();
                pushAlert('<div class="alert alert-success alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Person deleted</div>');
            }
            else {
                pushAlert('<div class="alert alert-warning alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' + data + '</div>');
            }
        });
    }
    $scope.ngAddTask = function (row) {
        document.getElementById('newTaskOwner').value = row.entity.ID;
    }
    $scope.ngEditPerson = function (row) {
        document.getElementById('editPersonID').value = row.entity.ID;
        document.getElementById('editPersonName').value = row.entity.name;
    }

    $scope.ngDetailPerson = function (row) {
        $("#collapseOne").collapse('hide');
        document.getElementById('editTaskOwnerID').value = row.entity.ID;
        detailPerson(row.entity.ID, function (data) {
            angular.element($("#detailPersonModal")).scope().refresh(data);

        });
    }


});

app.controller('TaskCtrl', function ($scope, $http) {
    $http.defaults.useXDomain = true;
    $http.get('http://crown.azurewebsites.net/home/TaskListView').success(function (data, status, headers, config) {
        $scope.myData = data;
    }).error(function (data, status, headers, config) {
        alert("fail");
    });
    $scope.gridOptions = {
        data: 'myData',
        enableCellSelection: false,
        enableRowSelection: false,
        columnDefs: [
            { field: '', displayName: '', width: 40 },
            { field: 'ID', displayName: 'ID' },
            { field: 'StartDate', displayName: 'Start Date', cellFilter: "date:\'MM/dd/yyyy\'" },
            { field: 'EndDate', displayName: 'End Date', cellFilter: "date:\'MM/dd/yyyy\'" },
            { field: 'Title', displayName: 'Title' },
            { field: 'personID', displayName: 'Task Owner ID' }
        ]
    };
    $scope.deletePerson = function (row) {
        deletePerson(row.entity.ID);
    }
    $scope.refresh = function () {
        if (!$scope.$$phase) {
            $http.get('http://crown.azurewebsites.net/home/TaskListView').success(function (data, status, headers, config) {
                $scope.myData = data;
                $scope.apply();

            }).error(function (data, status, headers, config) {
                alert("fail");
            });
        }
    }
});

app.controller('PersonDetailCtrl', function populateDetailPerson($scope) {
    $scope.refresh = function (data) {
        $scope.myData = JSON.parse(data);
        if (!$scope.$$phase) {
            $scope.$apply();
        }
    }
    $scope.mySelection = [];

    $scope.gridOptions = {
        data: 'myData',
        showSelectionCheckbox: true,
        selectedItems: $scope.mySelection,
        enableCellSelection: false,
        enableRowSelection: true,
        columnDefs: [
            { field: 'ID', displayName: 'ID', width: 50 },

            { field: 'StartDate', displayName: 'Start Date', cellFilter: "date:\'MM/dd/yyyy\'" },
            { field: 'EndDate', displayName: 'End Date', cellFilter: "date:\'MM/dd/yyyy\'" },
            { field: 'Title', displayName: 'Title' },
            { field: 'personID', displayName: 'Task Owner ID' },
            { field: '', displayName: '', cellTemplate: '<button class="btn btn-default delete-btn" ng-click="ngEditTask(row)"><span class="glyphicon glyphicon-pencil"></span></button>', width: 15 }
        ]
    };
    $scope.ngEditTask = function (row) {
        $("#editTaskID").val(row.entity.ID);
        $("#editTaskTitle").val(row.entity.Title);
        $("#editTaskStartDate").val(row.entity.StartDate);
        $("#editTaskEndDate").val(row.entity.EndDate);
        $("#collapseOne").collapse('hide');
        setTimeout(function () { $("#collapseOne").collapse('show') }, 700);

    }

    $scope.ngDeleteTask = function () {

        deleteTask($scope.mySelection);
        while ($scope.mySelection.length > 0) {
            $scope.mySelection.splice(0, 1);
        }
    }
});

function callController(url, method, data, callback) {
    var request = new XMLHttpRequest();
    request.onload = function () {
        var status = request.status;   
    }
    request.open(method, "http://crown.azurewebsites.net/home/" + url, true);
    request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    request.send(data);
    request.onreadystatechange = function () {
        if (request.status == 200 && request.readyState == 4) {

            callback(request.responseText);
        }
    }
}

function createPerson() {
    var newPersonName = document.getElementById("newPersonName").value;
    callController("createPerson", "POST", "name=" + newPersonName, function () {
        angular.element($("#personGrid")).scope().refresh();
    });
}

function editPerson() {
    var editPersonName = document.getElementById("editPersonName").value;
    var editPersonID = document.getElementById("editPersonID").value;
    callController("editPerson", "POST", "name=" + editPersonName+"&ID=" + editPersonID, function () {
        angular.element($("#personGrid")).scope().refresh();
    }); 
}

function detailPerson(id, callback) {
    callController("personDetailView/?ID=" + id, "GET", "", function (data) {
        callback(data);
    });

}

function createTask() {
    var ID = document.getElementById("newTaskOwner").value;
    var startDate = document.getElementById("newTaskStartDate").value;
    var endDate = document.getElementById("newTaskEndDate").value;
    var title = document.getElementById("newTaskTitle").value;
    var dataString = "personID="+ID+"&StartDate="+startDate+"&EndDate="+endDate+"&Title="+title;
    callController("createTask", "POST", dataString, function () {
        angular.element($("#taskGrid")).scope().refresh();
    });
}

function saveEditTask() {
    var ID = document.getElementById("editTaskID").value;
    var startDate = document.getElementById("editTaskStartDate").value;
    var endDate = document.getElementById("editTaskEndDate").value;
    var title = document.getElementById("editTaskTitle").value;
    var dataString = "ID=" + ID + "&StartDate=" + startDate + "&EndDate=" + endDate + "&Title=" + title;
    callController("editTask", "POST", dataString, function () {
        $("#collapseOne").collapse('hide');
        detailPerson(document.getElementById("editTaskOwnerID").value, function (data) {
            while (angular.element($("#PersonDetailGrid")).scope().mySelection.length > 0) {
                angular.element($("#PersonDetailGrid")).scope().mySelection.splice(0, 1);
            }
            angular.element($("#PersonDetailGrid")).scope().refresh(data);
            
        });
    });
}


function deleteTask(IdList) {
    if (IdList.length > 0) {
        var data = "";
        for (i = 0; i < IdList.length; ++i) {
            if (i == 0) {
                data += "id=";
            }
            else {
                data += "&id=";
            }
            data += IdList[i].ID;

        }
        callController("deleteTask", "POST", data, function () {
            angular.element($("#taskGrid")).scope().refresh();
            pushAlert('<div class="alert alert-info alert-dismissable"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>Task(s) deleted</div>');
        });
    }
}


function showPersonGrid() {
    $("#personGridPage").show();
    $("#taskGridPage").hide();
    var test = document.querySelector(".nav li");
    test.innerHTML = "User List";
}

function showTaskGrid() {
    $("#personGridPage").hide();
    $("#taskGridPage").show();
    var test = document.querySelector(".nav li");
    test.innerHTML = "Task List";
}

$(function () {
    $("#newTaskStartDate").datepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        todayHighlight: true,
        todayBtn: "linked"
    });
});

$(function () {
    $("#newTaskEndDate").datepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        todayHighlight: true,
        todayBtn: "linked"
    });
});

$(function () {
    $("#editTaskStartDate").datepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        todayHighlight: true,
        todayBtn: "linked"
    });
});

$(function () {
    $("#editTaskEndDate").datepicker({
        format: "yyyy-mm-dd",
        autoclose: true,
        todayHighlight: true,
        todayBtn: "linked"
    });
});

function pushAlert(message) {
    var alertHTML = message;
    document.getElementById('alertArea').innerHTML += alertHTML;
}